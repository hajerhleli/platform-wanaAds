import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'bootstrap/dist/css/bootstrap.css';
import './css/login.css';
import './css/campaigns.css';
import './css/products.css';
import 'semantic-ui-css/semantic.min.css';
import {Provider} from 'react-redux';
import store from './js/reducers/Store';

ReactDOM.render(
    <Provider store={store}>
      <App/>
    </Provider>,
    document.getElementById('root')
);
