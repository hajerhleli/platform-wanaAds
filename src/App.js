import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Register from "./js/components/Register";
import Login from "./js/components/login"
import Logout from "./js/components/Logout"
import Homepage from "./js/components/homepage"
import Root from "./js/components/Root"
import BasicWizard from "./js/components/BasicWizard"
import createBrowserHistory from 'history/createBrowserHistory'
import PropTypes from 'prop-types'
import {connect} from "react-redux"
import './App.css'
import {Payment} from "./js/components/Payment"
import Profile from "./js/components/Profile"
import editCampaign from "./js/components/editCampaign"

class App extends Component {
    constructor() {
        super();


    }
    componentWillMount() {
    }
  render() {

    return (
        <Router history={createBrowserHistory()}>
          <div>
              <Route exact path='/' component={Root}/>
              <Route path='/login' component={Login}/>
              <Route path='/register' component={Register}/>
              <Route path='/new/campaign' component={BasicWizard}/>
              <Route path='/logout' component={Logout}/>
              <Route exact path='/campaigns' component={Homepage}/>
              <Route path='/payment' component={Payment}/>
              <Route path='/profile' component={Profile}/>
              <Route exact path='/campaign/:id' component={editCampaign}/>

          </div>
        </Router>
    );
  }
}
App.propTypes = {
    campaigns: PropTypes.object
};

function mapStateToProps(state) {
  return {
      campaigns: state.user,
  }
}

export default connect(mapStateToProps, null)(App);