/**
 * Created by hajer on 04/10/17.
 */
import React, { Component } from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {addUser} from '../reducers/ContentActions'

class Register extends Component {
    state = {
        type: 'advertiser',
        // username: '',
        email: '',
        plainPassword: '',
        firstName: '',
        lastName: '',
        companyName: '',
        companyPhoneNumber: '',
        companyAddress: '',
        submittedtype: 'advertiser',
        submittedusername: false,
        submittedemail: false,
        submittedplainPassword: false,
        submittedfirstName: false,
        submittedlastName: false,
        submittedcompanyName: false,
        submittedcompanyPhoneNumber: false,
        submittedcompanyAddress: false,
        selected: false,
    }

    handleChange = (e, { name, value }) => this.setState({ [name]: value })

    testFields(field){
        if (field === ''){
            return true
        }
        else
            return false
    }

    handleSubmit = () => {
        let data = new FormData()
        this.state.selected = true
        console.log(this.state)
        // this.state.submittedusername= this.testFields(this.state.username)
        this.state.submittedemail = this.testFields(this.state.email)
        this.state.submittedplainPassword = this.testFields(this.state.plainPassword)
        this.state.submittedfirstName = this.testFields(this.state.firstName)
        this.state.submittedlastName = this.testFields(this.state.lastName)
        this.state.submittedcompanyName = this.testFields(this.state.companyName)
        this.state.submittedcompanyPhoneNumber = this.testFields(this.state.companyPhoneNumber)
        this.state.submittedcompanyAddress = this.testFields(this.state.companyAddress)
        let formData = {
            type: 'advertiser',
            // username: this.state.username,
            email: this.state.email,
            plainPassword: this.state.plainPassword,
            informations: {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                companyName: this.state.companyName,
                companyPhoneNumber: this.state.companyPhoneNumber,
                companyAddress: this.state.companyAddress
            }
        }
        var obj = this.toFormData(formData)
        console.log(obj)
        this.props.dispatch(addUser(obj));
        if (this.props.user.fetched === true) {

        }
    }

    toFormData(obj, form, namespace) {
        let fd = form || new FormData();
        let formKey;

        for (let property in obj) {
            if (obj.hasOwnProperty(property) && obj[property]) {
                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File, use recursivity.
                if (obj[property] instanceof Date) {
                    fd.append(formKey, obj[property].toISOString());
                }
                else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                    this.toFormData(obj[property], fd, formKey);
                } else { // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }
            }
        }

        return fd;
    }


    render() {

        const {
            user
        } = this.props;

        const { firstName, lastName, email,
                plainPassword, companyName, companyPhoneNumber,
                companyAddress
                } = this.state
        let error={
            email: false,
            plainPassword: false,
            informations: {
                firstName: false,
                lastName: false,
                companyName: false,
                companyPhoneNumber: false,
                companyAddress: false
            }
        };
        if(this.props.user.fetched === false){
            console.log('not fetched')
            error ={
                email: user.error.email !== undefined ,
                plainPassword: user.error.plainPassword !== undefined,
                informations: {
                    firstName:user.error.informations !== undefined && user.error.informations.firstName !== undefined,
                    lastName: user.error.informations !== undefined && user.error.informations.lastName !== undefined,
                    companyName: user.error.informations !== undefined && user.error.informations.companyName !== undefined,
                    companyPhoneNumber:user.error.informations !== undefined && user.error.informations.companyPhoneNumber !== undefined,
                    companyAddress:user.error.informations !== undefined && user.error.informations.companyAddress !== undefined
                }
            }
            this.state.selected = false
            var errorMessageToShow
            for ( var key in user.error ) {
                console.log(user.error[key])
                for (var k in user.error[key]){
                    console.log(user.error[key][k][0])
                    console.log(k)


                    errorMessageToShow= (k.replace(/([A-Z])/g, ' $1')
                        // uppercase the first character
                            .replace(/^./, function(str){ return str.toUpperCase(); }))+' : '+user.error[key][k][0]

                    console.log(errorMessageToShow)
                    break
                }

            }
        }
        return (
            <div className='login-form'>
                <Grid
                    textAlign='center'
                    style={{ height: '100%' }}
                    verticalAlign='middle'
                >
                    <Grid.Column style={{ maxWidth: 850 }}>

                        <Image src={require('../../picture/wanagames_logo_final.png')}  verticalAlign='middle'/>

                        <Form size='large' onSubmit={this.handleSubmit} encType="multipart/form-data" error>
                            <Segment stacked padded>

                                <Header as='h1'>Primary Contact</Header>
                                <Message hidden={user.fetched}
                                         error
                                         header={ errorMessageToShow }
                                />
                                <Form.Group widths='equal'>
                                    <Form.Field required>
                                        <Form.Input
                                            autoFocus={true}
                                            fluid
                                            icon='user'
                                            iconPosition='left'
                                            name='firstName'
                                            value={firstName}
                                            placeholder='First Name*'
                                            onChange={this.handleChange}
                                            error = {error.informations.firstName}
                                        />
                                    </Form.Field>

                                    <Form.Field required>
                                    <Form.Input
                                        fluid
                                        icon='user'
                                        iconPosition='left'
                                        name='lastName'
                                        value={lastName}
                                        placeholder='Last Name*'
                                        onChange={this.handleChange}
                                        error = {error.informations.lastName}
                                    />
                                    </Form.Field>
                                </Form.Group>

                                <Form.Field required>

                                </Form.Field>

                                <Form.Field required>
                                <Form.Input
                                    fluid
                                    icon='mail'
                                    iconPosition='left'
                                    name='email'
                                    value={email}
                                    placeholder='E-mail Address*'
                                    onChange={this.handleChange}
                                    error = {error.email}
                                />
                                </Form.Field>

                                <Form.Field required>
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    name='plainPassword'
                                    value={plainPassword}
                                    placeholder='Password*'
                                    type='password'
                                    onChange={this.handleChange}
                                    error = {error.plainPassword}
                                />
                                </Form.Field>
                                <Header as='h1'>Company</Header>
                                <Form.Group widths='equal'>
                                    <Form.Input
                                        fluid
                                        icon='id card'
                                        iconPosition='left'
                                        name='companyName'
                                        value={companyName}
                                        placeholder='Company Name'
                                        onChange={this.handleChange}
                                        error = {error.informations.companyName}
                                    />
                                    <Form.Input
                                        fluid
                                        icon='call'
                                        iconPosition='left'
                                        name='companyPhoneNumber'
                                        value={companyPhoneNumber}
                                        placeholder='Company Phone Number'
                                        onChange={this.handleChange}
                                        error = {error.informations.companyPhoneNumber}
                                    />
                                </Form.Group>

                                <Form.Input
                                    fluid
                                    icon='marker'
                                    iconPosition='left'
                                    name='companyAddress'
                                    value={companyAddress}
                                    placeholder='Company Address'
                                    onChange={this.handleChange}
                                    error = {error.informations.companyAddress}
                                />
                                <Button disabled={this.state.selected} fluid size='large'>Register</Button>
                            </Segment>
                        </Form>
                        <Message>
                            You already have account? <a href='/login'>Sign In</a>
                        </Message>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

Register.propTypes = {
    user: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.user
    };
}


export default connect(mapStateToProps, null)(Register);