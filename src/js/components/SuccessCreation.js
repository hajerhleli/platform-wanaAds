/**
 * Created by hajer on 16/11/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar} from './NavBar'
import { Segment} from 'semantic-ui-react'


class SuccessCreation extends Component {

    render() {

        return (

            <div >
                <NavBar />
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className=" col-xs-12" style={{textAlign: "center"}}>

                                    <Segment >
                                        <img  src={require('../../picture/checkmark.gif')}/>
                                        <h1>Congratulations!</h1>
                                        <h2>Your campaigns has been created succusfully.</h2>
                                    </Segment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        products: state.user
    }
}
export default connect(mapStateToProps, null)(SuccessCreation);