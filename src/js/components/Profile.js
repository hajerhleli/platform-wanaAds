/**
 * Created by hajer on 17/11/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar} from './NavBar'
import { Segment,Form,Header, Button} from 'semantic-ui-react'
import {getUser, updateUser} from '../reducers/ContentActions'


class Profile extends Component {

    constructor (props) {
        super()
        this.state = {
            loaded: false,
            email: '',
            firstName: '',
            lastName: '',
            companyName: '',
            companyPhoneNumber: '',
            companyAddress: '',
            submittedtype: 'advertiser',
            submittedusername: false,
            submittedemail: false,
            submittedplainPassword: false,
            submittedfirstName: false,
            submittedlastName: false,
            submittedcompanyName: false,
            submittedcompanyPhoneNumber: false,
            submittedcompanyAddress: false,
            selected: false,
        }
    }

    componentWillMount() {
        this.props.dispatch(getUser())
    }

    handleChange = (e, {name, value}) => {
        console.log(value)
        this.setState({[name]: value})
    }
    testNullFields(field){
        console.log(field)
        if (field === null){
            field = ''
        }
        console.log(field)
            return field
    }
    componentWillReceiveProps(nextProps) {
        if (nextProps !== this.props) {
            return true
        }
    }
    handleSubmit = () => {
        let data = new FormData()
        this.state.selected = true

        let formData = {
            email: this.state.email,
            informations: {
                firstName: this.testNullFields(this.state.firstName),
                lastName: this.testNullFields(this.state.lastName),
                companyName: this.testNullFields(this.state.companyName),
                companyPhoneNumber: this.testNullFields(this.state.companyPhoneNumber),
                companyAddress: this.testNullFields(this.state.companyAddress)
            }
        }
        var obj = this.toFormData(formData)
        console.log(obj)
        this.props.dispatch(updateUser(obj))
        if (this.componentWillReceiveProps()){
            console.log('test')
            this.state.selected = false
            this.componentWillMount()
        }
    }

    toFormData(obj, form, namespace) {
        let fd = form || new FormData();
        let formKey;

        for (let property in obj) {
            if (obj.hasOwnProperty(property) && obj[property]) {
                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File, use recursivity.
                if (obj[property] instanceof Date) {
                    fd.append(formKey, obj[property].toISOString());
                }
                else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                    this.toFormData(obj[property], fd, formKey);
                } else { // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }
            }
        }

        return fd;
    }


    render() {
        const {
            user
        } = this.props;
        console.log(this.state)


        if (this.props.user.fetched === true && !this.state.loaded){
            console.log('change state in render')
            console.log(this.props.user.user.data)
            this.state.firstName = this.props.user.user.data.informations.firstName
            this.state.lastName = this.props.user.user.data.informations.lastName
            this.state.email = this.props.user.user.data.email
            this.state.companyName = this.props.user.user.data.informations.companyName
            this.state.companyAddress = this.props.user.user.data.informations.companyAddress
            this.state.companyPhoneNumber = this.props.user.user.data.informations.companyPhoneNumber
            this.state.loaded = true
        }


        let error={
            email: false,
            plainPassword: false,
            informations: {
                firstName: false,
                lastName: false,
                companyName: false,
                companyPhoneNumber: false,
                companyAddress: false
            }
        };
        if(this.props.user.fetched === false){
            console.log('not fetched')
            error ={
                // username: user.error.username !== undefined,
                email: user.error.email.length !== 0,
                plainPassword: user.error.plainPassword !== undefined,
                informations: {
                    firstName:user.error.informations !== undefined && user.error.informations.firstName !== undefined,
                    lastName: user.error.informations !== undefined && user.error.informations.lastName !== undefined,
                    companyName: user.error.informations !== undefined && user.error.informations.companyName !== undefined,
                    companyPhoneNumber:user.error.informations !== undefined && user.error.informations.companyPhoneNumber !== undefined,
                    companyAddress:user.error.informations !== undefined && user.error.informations.companyAddress !== undefined
                }
            }
            this.state.selected = false
            console.log(error)
        }
        return (

            <div >
                <NavBar />
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className=" col-xs-12" >
                                <Segment stacked padded>
                                    <div className="row" style={{textAlign: 'center', marginBottom:"50px"}}>
                                        <h1>Personal Account</h1>
                                    </div>
                                    <Form size='large' onSubmit={this.handleSubmit} encType="multipart/form-data" error>

                                            <Header as='h1'>Primary Contact</Header>
                                                <Form.Field required>
                                                    <Form.Input
                                                        fluid
                                                        icon='user'
                                                        iconPosition='left'
                                                        name='firstName'
                                                        value={this.state.firstName}
                                                        placeholder='First Name*'
                                                        onChange={this.handleChange}
                                                        error = {error.informations.firstName}
                                                    />
                                                </Form.Field>

                                                <Form.Field required>
                                                    <Form.Input
                                                        fluid
                                                        icon='user'
                                                        iconPosition='left'
                                                        name='lastName'
                                                        value={this.state.lastName}
                                                        placeholder='Last Name*'
                                                        onChange={this.handleChange}
                                                        error = {error.informations.lastName}
                                                    />
                                                </Form.Field>
                                            {/*</Form.Group>*/}

                                            <Form.Field required>

                                            </Form.Field>

                                            <Form.Field required>
                                                <Form.Input
                                                    fluid
                                                    icon='mail'
                                                    iconPosition='left'
                                                    name='email'
                                                    value={this.state.email}
                                                    placeholder='E-mail Address*'
                                                    onChange={this.handleChange}
                                                    error = {error.email}
                                                />
                                            </Form.Field>

                                            <Header as='h1'>Company</Header>
                                                <Form.Input
                                                    fluid
                                                    icon='id card'
                                                    iconPosition='left'
                                                    name='companyName'
                                                    value={this.state.companyName}
                                                    placeholder='Company Name'
                                                    onChange={this.handleChange}
                                                    error = {error.informations.companyName}
                                                />
                                                <Form.Input
                                                    fluid
                                                    icon='call'
                                                    iconPosition='left'
                                                    name='companyPhoneNumber'
                                                    value={this.state.companyPhoneNumber}
                                                    placeholder='Company Phone Number'
                                                    onChange={this.handleChange}
                                                    error = {error.informations.companyPhoneNumber}
                                                />
                                            <Form.Input
                                                fluid
                                                icon='marker'
                                                iconPosition='left'
                                                name='companyAddress'
                                                value={this.state.companyAddress}
                                                placeholder='Company Address'
                                                onChange={this.handleChange}
                                                error = {error.informations.companyAddress}
                                            />
                                            <div className="row">
                                                <Button style={{ float: 'right', marginRight: '60px' }} disabled={this.state.selected} >Register</Button>
                                            </div>

                                    </Form>
                                </Segment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        user: state.user
    }
}
export default connect(mapStateToProps, null)(Profile);