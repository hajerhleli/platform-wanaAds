/**
 * Created by hajer on 19/10/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar} from './NavBar'
import {Button, Progress, Grid, Label, List, Form, Input, Image, Segment} from 'semantic-ui-react'
import {getAllProducts} from '../reducers/ContentActions'
import {ProductCard} from "./ProductCard";
import PropTypes from 'prop-types'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import     {createCampaign} from '../reducers/ContentActions'
class NewCampaigns extends Component {

    state = {
        completed: false,
        percent: 20,
        file: [],
        campaignName: '',
        errorCampaignName: false,
        product: {
            inProductImage: 'logo.png'
        },
        img: '',
        defineProduct: true,
        defineImg: false,
        errorPic: false,
        errorName: false,
        messageErrorPic: 'Only ".png" accepted',
        messageErrorName: ''
    }


    getInitialState() {
        const {endData = {}} = this.props;
        console.log(this.props)
        const {campaignName = '', file = [], percent} = endData;
        return {
            completed: (this.state.campaignName !== '') && (this.state.file.length !== 0),
            // campaignName: this.state.campaignName,
            // file:this.state.file,
            percent: 20,
            file: [],
            campaignName: '',
            errorCampaignName: false,
            product: {
                inProductImage: 'logo.png'
            },
            img: '',
            defineProduct: true,
            defineImg: false
        };
    }

    nextHandler() {
        console.log(this)
        this.state.percent = this.state.percent + 20
        const {completed, campaignName, file, percent, product} = this.state;

        if (!completed) {
            return;
        }

        const {startData} = this.props;
        var c = {
            name: this.state.campaignName,
            product: this.state.product.id,
            logo: {
                image: this.state.file
            },
            target: {
                ageInterval: {
                    minimum: 10,
                    maximum: 100
                }

            }

        }

        var obj = this.toFormData(c)
        console.log(obj)
        this.props.dispatch(createCampaign(obj))
        console.log(this.props)
        var th = this
        let interval = setInterval(function () {
            if (th.props.products.fetched) {
                let id = th.props.products.campaign.data.id
                // console.log(id)
                th.props.next(Object.assign({}, startData, {campaignName, file, percent, product,id}));
                clearInterval(interval)
            }
            else if(!th.props.products.fetched ){
                if (th.props.products.error.logo !== undefined) {
                    th.state.messageErrorPic = th.props.products.error.logo.image[0]
                    th.setState({errorPic: true})
                }
                if (th.props.products.error.name !== undefined) {
                    th.state.messageErrorName = th.props.products.error.name[0]
                    th.setState({errorName: true})
                }
                console.log(th.state)
                console.log("test")
                clearInterval(interval)
            }
        },1000)
    }

    renderButtons() {
        const {next, previous, cancel} = this.props;
        const {completed} = this.state;
        return (
            <div className='step-controls'>
                { previous !== undefined ? <button onClick={previous}>Previous</button> : null}
                <Button className="increment-btn"
                        onClick={this.nextHandler.bind(this)} disabled={!completed}>Next</Button>
                {/*<button onClick={cancel}>Cancel</button>*/}
            </div>
        );
    }

    toFormData(obj, form, namespace) {
        console.log(obj)
        let fd = form || new FormData();
        let formKey;

        for (let property in obj) {
            console.log(property)
            if (obj.hasOwnProperty(property) && obj[property]) {
                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File, use recursivity.
                if (obj[property] instanceof Date) {
                    fd.append(formKey, obj[property].toISOString());
                }
                else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                    this.toFormData(obj[property], fd, formKey);
                } else {
                    // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }
            }
        }
        return fd;
    }


    componentWillMount() {
        this.props.dispatch(getAllProducts());
    }

    handleChange = (e, {name, value}) => {
        if (value === '') {
            this.setState({errorCampaignName: true})
        } else if (this.state.file.length !== 0) {
            this.setState({completed: true})
            this.state.percent = this.state.percent + 20
        }
        if (value !== ''){
            this.setState({errorCampaignName: false})
            this.setState({errorName: false})
        }
        this.setState({[name]: value})
    }


    handleSubmit = () => {
        console.log(this.state)

    }

    selectProduct = (product) => {
        this.setState({product: product});
    }

    _onChange() {
        var file = this.refs.file.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);

        // }
        this.setState({
            defineImg: true
        })
        reader.onloadend = function (e) {

            console.log((this.refs.file.files[0]).type)
            if ((this.refs.file.files[0]).type !== "image/png") {
                this.setState({errorPic: true})
                this.setState({
                    file: []
                })
            } else {
                console.log((this.refs.file.files[0]))
                console.log('test png valid')
                this.setState({errorPic: false})
                this.setState({
                    img: [reader.result]
                })
                this.setState({
                    file: this.refs.file.files[0]
                })
                if (this.state.campaignName !== '') {
                    this.setState({completed: true})
                }
            }
        }.bind(this);

    }

    render() {
        let productsList
        let pic
        const {campaignName, file} = this.state;
        if (this.props.products.fetchedProducts === true) {

            productsList = this.props.products.products.data.map((product) =>
                <List.Item key={product.id} onClick={() => this.selectProduct(product)}>
                    <ProductCard
                        product={product}
                    />
                </List.Item>)
            var p
            for (p in this.props.products.products.data) {
                if (p < 1 && this.state.defineProduct) {
                    this.state.defineProduct = false
                    this.state.product = this.props.products.products.data[p]
                    this.state.img = <Image className="product-campaigns-img"
                                            src={'https://api.platform.wanagames.com/images/' + this.state.product.inProductImage}
                                            fluid/>

                }
                else
                    break
            }

        }
        console.log(this.state.product)
        return (

            <div >
                <NavBar />
                <div className="wrapper wrapper-steps">
                    <div className="container-fluid">
                        <div className="row progress-row">
                            <div className="col-xs-6 colmun-progress-selected">step 1</div>
                            <div className="col-xs-6 colmun-progress-unselected">step 2</div>
                        </div>
                        <div className="row">
                            <div className="column-products col-xs-2">
                                <List selection verticalAlign='middle'>
                                    {productsList}
                                </List>
                            </div>
                            <div className=" col-xs-7" style={{backgroundColor: "#ffffff", height: "660px"}}>

                                    <Segment >
                                        <div>
                                            <Form size='large' onSubmit={this.handleSubmit}
                                                  encType="multipart/form-data"
                                                  error>

                                                <Form.Input
                                                    required
                                                    fluid
                                                    iconPosition='left'
                                                    placeholder='Campaign Name'
                                                    name='campaignName'
                                                    value={campaignName}
                                                    onChange={this.handleChange}
                                                    error={this.state.errorCampaignName || this.state.errorName}

                                                />
                                                <div hidden={!this.state.errorName}>
                                                    <Label basic color='red' pointing>{this.state.messageErrorName}</Label>
                                                </div>
                                                <input
                                                    required
                                                    ref="file"
                                                    placeholder='Open File'
                                                    type='file'
                                                    value={file[0]}
                                                    onChange={this._onChange.bind(this)}
                                                    accept="image/png"

                                                />
                                                <img id="in_game4" src={this.state.img}
                                                     hidden={!this.state.defineImg}
                                                />
                                                <Image className="product-campaigns-img" width={'90%'}
                                                       src={'https://api.platform.wanagames.com/images/' + this.state.product.inProductImage}
                                                       fluid/>
                                                <div hidden={!this.state.errorPic}>
                                                    <Label basic color='red' pointing>{this.state.messageErrorPic}</Label>
                                                </div>
                                                { this.renderButtons() }
                                            </Form>
                                        </div>
                                    </Segment>
                            </div>
                            <div className="column-tips col-xs-3">
                                <List selection verticalAlign='middle'>
                                    <h1>Tips</h1>
                                </List>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        products: state.user,
        router: PropTypes.func
    }
}
NewCampaigns.contextTypes = {
    router: PropTypes.func
};
export default connect(mapStateToProps, null)(NewCampaigns);