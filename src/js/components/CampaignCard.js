/**
 * Created by hajer on 18/10/17.
 */
import React, { Component } from 'react';
import { Card, Icon, Image ,Grid} from 'semantic-ui-react'
import PropTypes from 'prop-types';
import {connect} from "react-redux";

export class CampaignCard extends Component {


    formatTime(number) {
        if (number < 10) {
            return '0' + number
        }

        return number;
    }
    render() {
        let time = this.formatTime(new Date(this.props.campaign.createdAt).getFullYear())+'-'+this.formatTime(new Date(this.props.campaign.createdAt).getMonth()+1)+'-'+this.formatTime(new Date(this.props.campaign.createdAt).getDate())

        return (
            <Grid.Column className="column-card-campaigns">
                <Card color='pink'>
                    <Image className="image-campaigns" src={'https://api.platform.wanagames.com/images/'+this.props.campaign.logo.image} />
                    <Card.Content>
                        <Card.Header>{this.props.campaign.name}</Card.Header>
                        <Card.Meta>{this.props.campaign.product.name}</Card.Meta>
                        <Card.Meta>Created at {time}</Card.Meta>
                    </Card.Content>
                    <Card.Content extra>
                        <a>
                            <Icon name='drivers license outline' />
                            More details
                        </a>
                    </Card.Content>
                </Card>
            </Grid.Column>
        );
    }
}
