/**
 * Created by hajer on 19/10/17.
 */
import React, { Component, PropTypes } from 'react'

export class Logout extends Component {

    render() {
        const { onLogoutClick } = this.props

        return (
            <button onClick={() => onLogoutClick()} className="btn btn-primary">
                Logout
            </button>
        )
    }

}
