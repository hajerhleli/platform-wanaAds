import React, { Component } from 'react';
import { Container, Dimmer,Loader } from 'semantic-ui-react'

import {connect} from "react-redux";
import {getAllProducts} from '../reducers/ContentActions'
import {testLogin} from '../reducers/ContentActions'

class Root extends Component {
    componentWillMount() {
        this.props.dispatch(testLogin());
    }
    render(){
        return(
        <div>
            <Dimmer active>
                <Loader size='big'>Loading</Loader>
            </Dimmer>
        </div>
        )
    }

}
function mapStateToProps(state) {
    return {
        user: state.user,
    }
}

export default connect(mapStateToProps, null)(Root);
