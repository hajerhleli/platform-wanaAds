/**
 * Created by hajer on 26/10/17.
 */
import React from 'react';
import ReadDOM from 'react-dom';
import Wizard from './Wizard';
import NewCampaigns from './NewCampaigns'
import Target from './Target'
import SuccessCreation from './SuccessCreation'

const BasicWizard = React.createClass({
    save: function(data) {
        console.log('saved', data);
    },
    cancel: function() {
        console.log('cancelled');
    },
    render: function() {
        const steps = [NewCampaigns, Target,SuccessCreation];
        return <Wizard steps={steps} save={this.save} cancel={this.cancel} initialData={{}}/>
    }
});

export default (BasicWizard);