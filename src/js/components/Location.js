/**
 * Created by hajer on 02/11/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import { Form, Dropdown } from 'semantic-ui-react'
import { getAllLocations } from '../reducers/ContentActions'
import { feature } from "topojson-client"
import  WorldMap from  './WorldMap'

class Location extends Component{
    constructor() {
        super();
        this.state = {
            worlddata: [],
            selected:[],
            targetValue: 100
        }


    }
    componentWillMount(){
        this.props.dispatch(getAllLocations());

    }
    handleChange = (e, {name, value}) => {
            this.props.handleLocation(value)
            this.state.selected = []
        for(var j in value){
            var indexOfId = (this.props.target.locations.data).findIndex(i => i.id === value[j]);
                    this.state.selected.push(this.props.target.locations.data[indexOfId])
                }
    }
    render(){
        var locationList = []
        if (this.props.target.fetchedLocation === true) {
            var p
            for (p in this.props.target.locations.data) {
                locationList.push({
                    text: this.props.target.locations.data[p].name,
                    value: this.props.target.locations.data[p].id,
                    key: this.props.target.locations.data[p].externalId,
                })
            }

        }
        return(
            <div>

                <Form.Field inline>
                <label>Location</label>

                <Dropdown
                    placeholder='Select a location'
                    fluid
                    multiple
                    selection
                    name="locations"
                    search
                    // value={this.state.locations}
                    options={locationList}
                    onChange={this.handleChange}
                />
                </Form.Field>
                <Form.Field>
                    <WorldMap
                        selectedData={this.state.selected}
                    />
                </Form.Field>
            </div>
        )
    }

}

function mapStateToProps(state) {
    return {
        target: state.user
    }
}
export default connect(mapStateToProps, null)(Location);