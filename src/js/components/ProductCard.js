/**
 * Created by hajer on 18/10/17.
 */
import React, { Component } from 'react';
import { Card, Icon, Image ,Grid} from 'semantic-ui-react'
import PropTypes from 'prop-types';
import {connect} from "react-redux";

export class ProductCard extends Component {


    render() {

        return (
            <div>
                <div className="product-row" >
                    <Image className="image-product"  src={'https://api.platform.wanagames.com/images/'+this.props.product.logo} />

                        <h4>{this.props.product.name}</h4>

                </div>
            </div>
        );
    }
}
