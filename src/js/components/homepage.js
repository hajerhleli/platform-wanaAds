import React, {Component} from 'react'
import {Grid, Container, Segment, Button} from 'semantic-ui-react'
import {getAllCampaigns,deleteCampaign} from '../reducers/ContentActions'
import PropTypes from 'prop-types'
import {connect} from "react-redux"
import {CampaignCard} from './CampaignCard'
import {NavBar} from './NavBar'
import 'bootstrap/dist/css/bootstrap.css'
import Datatable from 'react-bs-datatable'

class Homepage extends Component {

    state = {
        active:false
    }
    componentWillMount() {
        this.props.dispatch(getAllCampaigns());
    }
    shouldComponentUpdate(nextProps, nextState){
        return this.props !== nextProps
    }

    handleClose = () => this.setState({ active: false })
    deleteCampaign(id){
        console.log(id)
        this.props.dispatch(deleteCampaign(id))
        if (this.shouldComponentUpdate()) {
            console.log(this.shouldComponentUpdate())
            this.componentWillMount()
            this.forceUpdate()
        }
    }
    detailsCampaign(id){
        console.log(id)
        window.location = ('/campaign/'+id)
    }
    render() {
        const { active } = this.state
        let campaignsList
        let header = [
            { title: 'Product', prop: 'image', sortable: true},
            { title: 'Name', prop: 'name', sortable: true, filterable: true  },
            { title: 'Game', prop: 'productName' ,sortable: true, filterable: true },
            { title: 'Created At', prop: 'createdAt', sortable: true },
            { title: 'Analytics', prop: '', sortable: true },
            { title: 'Actions', prop: 'actions'},
        ];
        let body = []

        if (this.props.campaigns.fetched === true && this.props.campaigns.fetching === false) {

            campaignsList = this.props.campaigns.campaigns.data.map((campaign) =>
            {
                body.push({
                    image: <img className="icon-campaign" src={'https://api.platform.wanagames.com/images/'+campaign.logo.image} />,
                    name: campaign.name,
                    productName: <div>
                        <img className="icon-campaign" src={'https://api.platform.wanagames.com/images/'+campaign.product.logo} />
                        <p>{campaign.product.name}</p>
                        <p>{campaign.product.publisher.username}</p>

                    </div>,
                    createdAt: new Date(campaign.createdAt).toLocaleString(),
                    actions:<div>
                        <Button icon='eye' onClick={() => this.detailsCampaign(campaign.id)} />
                        <Button icon='delete'  onClick={() => this.deleteCampaign(campaign.id)} />
                    </div>
                        // .getYear()+'/'+new Date(campaign.createdAt).getMonth()+'/'+new Date(campaign.createdAt).getDate()
                })
            })


        }
        return (
            <div>

                <NavBar/>
                <div className="container-fluid">

                    <Grid columns={4}  >
                        <Grid.Row className="row-list">
                            <Segment raised padded>
                                <Datatable
                                    tableHeader={header}
                                    tableBody={body}
                                    keyName="userTable"
                                    tableClass="striped hover bordered responsive"
                                    rowsPerPage={5}
                                    rowsPerPageOption={[5, 10, 15, 20]}
                                    initialSort={{prop: "name", isAscending: true}}
                                />
                            </Segment>
                            {/*{campaignsList}*/}
                        </Grid.Row>
                    </Grid>
                </div>
            </div>
        );
    }
}

Homepage.propTypes = {
    campaigns: PropTypes.object
};

function mapStateToProps(state) {
    return {
        campaigns: state.user,
    }
}

export default connect(mapStateToProps, null)(Homepage);
