/**
 * Created by hajer on 04/10/17.
 */
import React, {Component} from 'react'
import {Button, Form, Grid, Header, Image, Message, Segment} from 'semantic-ui-react'
import {loginUser} from '../reducers/ContentActions'
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {BrowserRouter as router, Route, Switch} from 'react-router-dom';
import {testLogin} from '../reducers/ContentActions'

class Login extends Component {
    constructor() {
        super();
        this.state = {
            email: '',
            password: '',
            submittedEmail: '',
            submittedPassword: '',
            selected: false
        };

    }

    handleChange = (e, {name, value}) => this.setState({[name]: value})

    parseURLParams(url) {
        var queryStart = url.indexOf("?") + 1,
            queryEnd = url.indexOf("#") + 1 || url.length + 1,
            query = url.slice(queryStart, queryEnd - 1),
            pairs = query.replace(/\+/g, " ").split("&"),
            parms = {}, i, n, v, nv;

        if (query === url || query === "") return;

        for (i = 0; i < pairs.length; i++) {
            nv = pairs[i].split("=", 2);
            n = decodeURIComponent(nv[0]);
            v = decodeURIComponent(nv[1]);

            if (!parms.hasOwnProperty(n)) parms[n] = [];
            parms[n].push(nv.length === 2 ? v : null);
        }
        return parms;
    }

    handleSubmit = () => {
        var data = new FormData();
        this.state.selected = true
        this.setState({submittedEmail: this.state.email, submittedPassword: this.state.password})
        console.log(this.state)
        data.append('_username', this.state.email)
        data.append('_password', this.state.password)
        this.props.dispatch(loginUser(data));
    }

    render() {
        let urlString = window.location.href;
        let urlParams = this.parseURLParams(urlString);
        console.log(urlParams)
        let message = '';
        let showmessage = false;
        if (urlParams!== undefined){
             message = "You have successfully registered."
             showmessage = true;
        }
        console.log(message)
        const {
            user
        } = this.props;
        const {email, password} = this.state;
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        console.log(re.test(this.state.email))
        // show error message when login failed
        let error;
        if (this.props.user.fetched === false) {
            console.log('not fetched')
            error = <Message hidden={user.fetched}
                             error
                             header={user.error}
            />
            this.state.selected = false
        }
        if (this.props.user.fetched === true) {
        }

        return (
            <div className='login-form'>
                <div className="alert alert-success" hidden={!showmessage}>
                    <h1>
                        <strong>Thank You!</strong>

                    </h1>
                    <h2>
                        {message}
                    </h2>
                </div>
                <Grid
                    textAlign='center'
                    style={{height: '100%'}}
                    verticalAlign='middle'>

                    <Grid.Column style={{maxWidth: 550}}>

                        <Image src={require('../../picture/wanagames_logo_final.png')} verticalAlign='middle'/>

                        <Form size='large' onSubmit={this.handleSubmit} encType="multipart/form-data" error>
                            <Segment raised padded>
                                {error}
                                <Form.Input
                                    autoFocus={true}
                                    fluid
                                    icon='user'
                                    iconPosition='left'
                                    placeholder='E-mail address'
                                    name='email'
                                    value={email}
                                    onChange={this.handleChange}

                                />
                                <Form.Input
                                    fluid
                                    icon='lock'
                                    iconPosition='left'
                                    placeholder='Password'
                                    type='password'
                                    name='password'
                                    value={password}
                                    onChange={this.handleChange}
                                />

                                <Button disabled={this.state.selected} fluid size='large'>Login</Button>
                            </Segment>
                        </Form>
                        <Message>
                            New to us? <a href='/register'>Sign Up</a>
                        </Message>
                    </Grid.Column>
                </Grid>
            </div>
        );
    }
}

Login.propTypes = {
    user: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        user: state.user,
        router: React.PropTypes.object
    };
}


export default connect(mapStateToProps, null)(Login);
