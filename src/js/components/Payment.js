/**
 * Created by hajer on 17/11/17.
 */
import React, {Component} from 'react';
import {NavBar} from './NavBar'
import {Loader, Segment, Sticky} from 'semantic-ui-react'

export class Payment extends Component {

    render() {

        return (
            <div >
                <NavBar />
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className=" col-xs-12" >

                                <Sticky >
                                    <Segment style={{ height:'500px',
                                        textAlign: "center"}}>

                                        <h1>Coming soon ...</h1>
                                    </Segment>
                                </Sticky>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

