/**
 * Created by hajer on 19/10/17.
 */
import React, { Component } from 'react';
import { Menu,Dropdown,Icon} from 'semantic-ui-react'
import {Navbar,Nav, NavItem, NavDropdown, MenuItem,
        FormGroup, FormControl, Button, InputGroup} from 'react-bootstrap'

export class NavBar extends Component {
    handleLogout(){
        localStorage.removeItem('token')
        window.location.replace('/login')
    }
    handleCreation(){
        window.location.replace('/new/campaign')
    }
    handleFetch(){
        window.location.replace('/campaigns')
    }

    // handleBudget(){
    //     window.location.replace('/payment')
    // }

    render() {
        let username = localStorage.getItem('username')
        return (
                <Navbar >
                    <Navbar.Header>
                        <a className="navbar-brand">
                            <img className="img-header"  src={require('../../picture/logo.png')} />|
                        {/*</Navbar.Brand>*/}
                        {/*<Navbar.Brand>*/}


                            <Icon name="content" />
                            <p>
                                Ads Manager
                            </p>
                        </a>
                    </Navbar.Header>

                    <Nav pullRight >
                        <Navbar.Form pullLeft>
                            <FormGroup>

                                <InputGroup className="input-navbar">
                                    <FormControl type="text" placeholder="Search" />
                                    <InputGroup.Addon>
                                        <Icon name='search'  />
                                    </InputGroup.Addon>
                                </InputGroup>
                            </FormGroup>
                        </Navbar.Form>

                        <NavItem eventKey={1} onClick={this.handleFetch} >My Campaigns</NavItem>
                        <NavItem eventKey={2} onClick={this.handleCreation}>New Campaign</NavItem>
                        <NavDropdown eventKey={3} title={username} id="dropdown">
                            <MenuItem  id="basic-nav-dropdown" eventKey={3.1} href="/profile">Profile</MenuItem>
                            <MenuItem  id="basic-nav-dropdown" eventKey={3.2} href="/payment">Budget</MenuItem>
                        </NavDropdown>
                        <NavItem eventKey={4}>
                            <Icon  onClick={this.handleLogout} name='help' />
                            <Icon onClick={this.handleLogout} size='large' name='sign out'/>
                        </NavItem>
                    </Nav>
                </Navbar>
        )
    }
}
