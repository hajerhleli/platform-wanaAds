/**
 * Created by hajer on 26/10/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar} from './NavBar'
import {CampaignCard} from './CampaignCard'
import { Segment,Item} from 'semantic-ui-react'
import {getCampaign} from '../reducers/ContentActions'


class editCampaign extends Component {

    componentWillMount() {
        this.props.dispatch(getCampaign(this.props.match.params.id))
    }

    render() {
        console.log(this.props)
        var campaignCard
        let campaignItem
        console.log(this.props.campaign)
        if (this.props.campaign.fetched === true && this.props.campaign.fetching === false) {
            campaignItem =
                <Item.Group>
                    <Item>
                        <Item.Image size='tiny' src={'https://platform.mate.tn/images/'+this.props.campaign.campaign.data.logo.image} />

                        <Item.Content>
                            <Item.Header>{ this.props.campaign.campaign.data.name}</Item.Header>
                            <Item.Meta>{ this.props.campaign.campaign.data.product.name}</Item.Meta>
                            <Item.Meta>{ this.props.campaign.campaign.data.product.publisher.username}</Item.Meta>
                            <Item.Description>
                                <img src={'https://platform.mate.tn/images/'+this.props.campaign.campaign.data.product.inProductImage} />
                            </Item.Description>
                            <Item.Extra>{this.props.campaign.campaign.data.createdAt}</Item.Extra>
                        </Item.Content>
                    </Item>

                </Item.Group>
            campaignCard = <CampaignCard campaign={this.props.campaign.campaign.data}/>

        }
        return (

            <div >
                <NavBar />
                <div className="wrapper">
                    <div className="container-fluid">
                        <div className="row">
                            <div className=" col-xs-12" >

                                    <Segment style={{marginTop:'20px',padding:'20px'}}>
                                        {campaignItem}
                                    </Segment>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        campaign: state.user
    }
}
export default connect(mapStateToProps, null)(editCampaign);
