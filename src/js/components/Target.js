/**
 * Created by hajer on 20/10/17.
 */
import React, {Component} from 'react';
import {connect} from "react-redux";
import {NavBar} from './NavBar'
import {Button, Input, Select, Container, Label, Form, Dropdown, Image, Segment} from 'semantic-ui-react'
import PropTypes from 'prop-types'
import {
    getAllGenders,
    getAllCategories,
    getAllInterests,
    getAllLanguages,
    getAllLocations,
    updateCampaign,
    getTargetsValue
} from '../reducers/ContentActions'
import {feature} from "topojson-client"
import CircularProgressbar from 'react-circular-progressbar';
import  WorldMap from  './WorldMap'
import  Location from  './Location'


class Target extends Component {
    constructor() {
        super();
        this.state = {
            worlddata: [],
            selected: [],
            error: false
        }


    }

    componentWillMount() {
        this.props.dispatch(getAllGenders());
        this.props.dispatch(getAllCategories());
        this.props.dispatch(getAllInterests());
        this.props.dispatch(getAllLanguages());
        // this.props.dispatch(getAllLocations());
        const {startData, previous, cancel} = this.props;
        const {campaignName, file} = startData;
        console.log(campaignName)
        console.log(this.props)

        this.state = {
            percent: this.props.startData.percent,
            campaignName: this.props.startData.campaignName,
            file: this.props.startData.file,
            locations: [],
            categories: [],
            genders: [],
            minimum: 13,
            maximum: 100,
            targetValue: 100,
            amount: 20

        }


        let data = {
            categories: this.state.categories,
            languages: this.state.languages,
            genders: this.state.genders,
            locations: this.state.locations,
            minAge: this.state.minimum,
            maxAge: this.state.maximum

        }
        console.log(data)
        this.props.dispatch(getTargetsValue(data))
        // console.log(this.props.location.state)
        console.log(this.state)
    }

    handleChange = (e, {name, value}) => {
        this.setState({[name]: value}, function () {
            let data = {
                categories: this.state.categories,
                languages: this.state.languages,
                genders: this.state.genders,
                locations: this.state.locations,
                minAge: this.state.minimum,
                maxAge: this.state.maximum

            }
            this.props.dispatch(getTargetsValue(data))
            this.state.targetValue = this.props.target.targetValue.data.percentage


            if (name === "locations") {
                this.state.selected = []
                for (var j in this.state.locations) {
                    var indexOfId = (this.props.target.locations.data).findIndex(i => i.id === this.state.locations[j]);
                    this.state.selected.push(this.props.target.locations.data[indexOfId])
                    console.log(this.props.target.locations.data[indexOfId])
                    console.log("**************************")
                    console.log(this.state.locations)
                    console.log(this.state.selected)
                }

            }
        })


    }

    handleSubmit = () => {
        var c = {
            name: this.state.campaignName,
            amount: this.state.amount,
            product: this.props.startData.product.id,
            logo: {
                image: this.state.file
            },
            target: {
                categories: this.state.categories,
                interests: this.state.interests,
                locations: this.state.locations,
                languages: this.state.languages,
                genders: this.state.genders,
                ageInterval: {
                    minimum: this.state.minimum,
                    maximum: this.state.maximum
                }

            }

        }
        console.log(c)
        var obj = this.toFormData(c)
        console.log(obj)
        this.props.dispatch(updateCampaign(obj, this.props.startData.id))
        const {startData} = this.props;

        var th = this
        let interval = setInterval(function () {
            th.props.next(Object.assign({}, startData, {}));
            clearInterval(interval)
        }, 1000)
        // }
    }
    increment = () => {
        this.setState({
            percent: this.state.percent >= 100 ? 0 : this.state.percent + 20
        })
    }

    toFormData(obj, form, namespace) {
        console.log(obj)
        let fd = form || new FormData();
        let formKey;

        for (let property in obj) {
            console.log(property)
            if (obj.hasOwnProperty(property) && obj[property]) {
                if (namespace) {
                    formKey = namespace + '[' + property + ']';
                } else {
                    formKey = property;
                }

                // if the property is an object, but not a File, use recursivity.
                if (obj[property] instanceof Date) {
                    fd.append(formKey, obj[property].toISOString());
                }
                else if (typeof obj[property] === 'object' && !(obj[property] instanceof File)) {
                    this.toFormData(obj[property], fd, formKey);
                } else {
                    // if it's a string or a File object
                    fd.append(formKey, obj[property]);
                }
            }
        }
        return fd;
    }

    handleLocation(value) {
        console.log(value)
        this.setState({locations: value}, function () {
            let data = {
                categories: this.state.categories,
                languages: this.state.languages,
                genders: this.state.genders,
                locations: this.state.locations,
                minAge: this.state.minimum,
                maxAge: this.state.maximum

            }
            this.props.dispatch(getTargetsValue(data))
            this.state.targetValue = this.props.target.targetValue.data.percentage
        })
    }

    render() {
        console.log(this.state)
        var statistic1 = ''
        var statistic2 = ''
        if (this.props.target.targetValue !== undefined) {
            this.state.targetValue = Number((this.props.target.targetValue.data.percentage).toFixed(2))
            console.log(Number((this.props.target.targetValue.data.percentage).toFixed(2)))
            statistic1 = this.props.target.targetValue.data.selected
            statistic2 = this.props.target.targetValue.data.total
        }

        var languagesList = []
        if (this.props.target.fetchedLanguages === true) {
            var p
            for (p in this.props.target.languages.data) {
                languagesList.push({
                    text: this.props.target.languages.data[p].name,
                    value: this.props.target.languages.data[p].id,
                })
            }

        }
        var intrestesList = []
        if (this.props.target.fetchedInterests === true) {
            var p
            for (p in this.props.target.interests.data) {
                intrestesList.push({
                    text: this.props.target.interests.data[p].name,
                    value: this.props.target.interests.data[p].id,
                })
            }

        }
        var categoriesList = []
        if (this.props.target.fetchedCategories === true) {
            var p
            for (p in this.props.target.categories.data) {
                categoriesList.push({
                    text: this.props.target.categories.data[p].name,
                    value: this.props.target.categories.data[p].id,
                })
            }

        }
        var gendersList = []
        if (this.props.target.fetchedGenders === true) {
            var p
            for (p in this.props.target.genders.data) {

                gendersList.push({
                    text: this.props.target.genders.data[p].name,
                    value: this.props.target.genders.data[p].id,
                })
            }
        }
        let budgetType =  [
            { key: 'db', value: 'Daily Budget',  text: 'Daily Budget' },
            { key: 'lf', value: 'A Life Budget', text: 'A Life Budget' }
        ]
        // let budgetType =['Daily Budget', 'A Life Budget']
        return (
            <div>
                <NavBar />
                <div className="wrapper">
                    <div className="container">
                        <div className="row progress-row">
                            <div className="col-xs-6 colmun-progress-selected">step 1</div>
                            <div className="col-xs-6 colmun-progress-selected">step 2</div>
                        </div>
                        <div className="row">
                            <div className="column-tips-target col-xs-9">
                                <Segment raised>
                                    <div >

                                        <Form size='large' onSubmit={this.handleSubmit} encType="multipart/form-data"
                                              error>
                                            <Form.Field inline>
                                            <label >Budget</label>
                                            <Select options={budgetType} />
                                            <Input
                                                min="20"
                                                type='number'
                                                iconPosition='right'
                                                icon="pound"
                                                placeholder='Budget'
                                                name="amount"
                                                value = {this.state.amount}
                                                onChange={this.handleChange}
                                            />
                                            </Form.Field>
                                            <Location locations={this.state.locations}
                                                      handleLocation={this.handleLocation.bind(this)}/>
                                            <Form.Field inline>
                                            <label>Age</label>
                                                <Input
                                                    id="min"
                                                    min="0"
                                                    type='number'
                                                    iconPosition='left'
                                                    placeholder='Minimum age'
                                                    name='minimum'
                                                    value={this.state.minimum}
                                                    onChange={this.handleChange}

                                                />

                                                <Input
                                                    min={this.state.minimum}
                                                    type='number'
                                                    iconPosition='left'
                                                    placeholder='Maximum age'
                                                    name='maximum'
                                                    value={this.state.maximum}
                                                    onChange={this.handleChange}

                                                />
                                            </Form.Field>
                                            <Form.Field inline>
                                                <label>Gender</label>
                                                <Dropdown
                                                    placeholder='Select a gender'
                                                    multiple
                                                    selection
                                                    name="genders"
                                                    options={gendersList}
                                                    onChange={this.handleChange}
                                                />
                                            </Form.Field>
                                            <Form.Field inline>
                                            <label>Languages</label>
                                            <Dropdown
                                                placeholder='Select a language'
                                                multiple
                                                selection
                                                name="languages"
                                                options={languagesList}
                                                onChange={this.handleChange}
                                            />
                                            </Form.Field>
                                            <Form.Field inline>
                                            <label>Categories</label>
                                            <Dropdown
                                                placeholder='Select a category'
                                                multiple
                                                selection
                                                search
                                                name="categories"
                                                options={categoriesList}
                                                onChange={this.handleChange}
                                            />

                                            </Form.Field>

                                            <Button className="increment-btn"
                                                    onClick={this.increment}>Create</Button>

                                        </Form>
                                    </div>

                                </Segment>
                            </div>
                            <div className="column-products col-xs-3">
                                <div className="count-circle">
                                    <CircularProgressbar percentage={this.state.targetValue}
                                                         strokeWidth={5}
                                                         className="CircularProgressbar-inverted"
                                                         initialAnimation='10000ms'/>
                                </div>
                                <h2>Estimated reach per percent.</h2>
                                <h4>{statistic1 + '\/' + statistic2}</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        target: state.user
    }
}
export default connect(mapStateToProps, null)(Target);