// src/components/WorldMap.js

import React, {Component} from "react"
import {geoMercator, geoPath} from "d3-geo"
import {feature} from "topojson-client"

class WorldMap extends Component {
    constructor() {
        super()
        this.state = {
            worlddata: [],
            selected: []
        }

        this.handleCountryClick = this.handleCountryClick.bind(this)
        this.handleMarkerClick = this.handleMarkerClick.bind(this)
    }

    projection() {
        return geoMercator()
            .scale(250)
            .translate([1600 / 2, 900 / 2])
    }

    handleCountryClick(countryIndex, th) {
        // // console.log("Clicked on country: ", this.state.worlddata[countryIndex])
        // var indexOfId = (this.state.worlddata).findIndex(i => i.id === "788");
        // // // console.log(indexOfId)
        // // // console.log("Clicked on country: ", this.state.worlddata[indexOfId])
        //
        // // // console.log(countryIndex)
        // // // console.log(th.target.className)
        // th.target.className.baseVal = "country-selected"
    }

    handleMarkerClick(markerIndex) {
        // // console.log("Marker: ", this.state.cities[markerIndex])
    }

    componentDidMount() {
        fetch("https://unpkg.com/world-atlas@1/world/110m.json")
            .then(response => {
                if (response.status !== 200) {
                    // // console.log(`There was a problem: ${response.status}`)
                    return
                }
                response.json().then(worlddata => {
                    this.setState({
                        worlddata: feature(worlddata, worlddata.objects.countries).features,
                    })
                })
            })
    }

    render() {
        // // console.log(this.props.selectedData)
        this.state.selected = []
        for (var j in this.props.selectedData) {
            // console.log(this.props.selectedData[j].externalId)
            // console.log(((this.props.selectedData[j].externalId).toString()).length)
            var index
            if (((this.props.selectedData[j].externalId).toString()).length === 2) {
                index = '0'.concat((this.props.selectedData[j].externalId))
            }
            if (((this.props.selectedData[j].externalId).toString()).length === 1) {
                index = '00'.concat((this.props.selectedData[j].externalId))
            }
            if (((this.props.selectedData[j].externalId).toString()).length === 3){
                index = (this.props.selectedData[j].externalId).toString()
            }
            this.state.selected.push(index)
            // console.log(index)
        }
        // console.log(this.state.selected)
        var world = this.state.worlddata.map((d, i) => (
                <path
                    key={ `path-${ i }` }
                    d={ geoPath().projection(this.projection())(d) }
                    className="country"
                    fill={ `rgb(75, 37, 58)` }
                    stroke="#FFFFFF"
                    strokeWidth={ 0.5 }
                    value={i}
                    // onClick={this.handleCountryClick.bind(this, i) }
                />
        ))
        var list = []
        for (let i in this.state.worlddata) {
            // console.log(((this.state.selected).findIndex(j => j === this.state.worlddata[i].id)))
            if(((this.state.selected).findIndex(j => j === this.state.worlddata[i].id))!== -1){
                    list.push(
                        <path
                            key={ `path-${ i }` }
                            d={ geoPath().projection(this.projection())(this.state.worlddata[i]) }
                            className="country-selected"
                            fill={ `rgb(75, 37, 58)` }
                            stroke="#FFFFFF"
                            strokeWidth={ 0.5 }
                            value={i}
                            // onClick={this.handleCountryClick.bind(this, i) }
                        />
                    )
                }
                else {

                list.push(
                        <path
                            key={ `path-${ i }` }
                            d={ geoPath().projection(this.projection())(this.state.worlddata[i]) }
                            className="country"
                            fill={ `rgb(75, 37, 58)` }
                            stroke="#FFFFFF"
                            strokeWidth={ 0.5 }
                            value={i}
                            onClick={this.handleCountryClick.bind(this, i) }
                        />
                    )
                }

        }




        return (
            <div>
                <svg width={ '100%' } height={ 350 } viewBox="0 0 1600 900">
                    <g className="countries">
                        {
                            list.map((j) => (j))
                        }
                </g>
                </svg>

            </div>
        )
    }
}

export default WorldMap