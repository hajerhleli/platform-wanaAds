export default function reducer(state = {
  loaded: false,
  fetching: false,
}, action) {
    switch (action.type) {
        case 'FETCH_USER_START':
            return {
                ...state,
                fetching: true,
                isAuthenticated: false,
                user: undefined
            }
        case 'FETCH_USER_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_USER_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                isAuthenticated: true,
                user: action.payload
            }
        case 'FETCH_USER_REGISTRATION_START':
            return {
                ...state,
                fetching: true,
                user: undefined
            }
        case 'FETCH_USER_REGISTRATION_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                message: action.payload,
                error: action.payload
            }
        case 'FETCH_USER_REGISTRATION_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                user: action.payload
            }

        case 'FETCH_USER_CAMPAIGNS_START':
            return {
                ...state,
                fetching: true,
                user: undefined
            }
        case 'FETCH_USER_CAMPAIGNS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                error: action.payload
            }
        case 'FETCH_USER_CAMPAIGNS_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                campaigns: action.payload
            }
        case 'FETCH_PRODUCTS_START':
            return {
                ...state,
                fetching: true,
                fetchedProducts: false,
                user: undefined
            }
        case 'FETCH_PRODUCTS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedProducts: false,
                error: action.payload
            }
        case 'FETCH_PRODUCTS_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedProducts: true,
                products: action.payload
            }
        case 'FETCH_GENDERS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedGenders: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_GENDERS_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedGenders: true,
                isAuthenticated: true,
                genders: action.payload
            }
        case 'FETCH_GENDERS_START':
            return {
                ...state,
                fetching: true,
                fetchedGenders: false,
                genders: undefined
            }
        case 'FETCH_CATEGORIES_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedCategories: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_CATEGORIES_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedCategories: true,
                isAuthenticated: true,
                categories: action.payload
            }
        case 'FETCH_CATEGORIES_START':
            return {
                ...state,
                fetching: true,
                fetchedCategories: false,
                categories: undefined
            }
        case 'FETCH_INTERESTS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedInterests: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_INTERESTS_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedInterests: true,
                isAuthenticated: true,
                interests: action.payload
            }
        case 'FETCH_INTERESTS_START':
            return {
                ...state,
                fetching: true,
                fetchedInterests: false,
                interests: undefined
            }
        case 'FETCH_LANGUAGES_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedLanguages: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_LANGUAGES_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedLanguages: true,
                isAuthenticated: true,
                languages: action.payload
            }
        case 'FETCH_LANGUAGES_START':
            return {
                ...state,
                fetching: true,
                fetchedLanguages: false,
                languages: undefined
            }
        case 'FETCH_LOCATIONS_REJECTED':
            return {
                ...state,
                fetching: false,
                fetchedLocation: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_LOCATIONS_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetchedLocation: true,
                isAuthenticated: true,
                locations: action.payload
            }
        case 'FETCH_LOCATIONS_START':
            return {
                ...state,
                fetching: true,
                fetchedLocation: false,
                locations: undefined
            }
        case 'PASS_CAMPAIGN_FULLIFIED' :
            return {
                ...state,
                newcampaign: action.payload
            }
        case 'CREATE_NEW_CAMPAIGN_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'CREATE_NEW_CAMPAIGN_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                isAuthenticated: true,
                campaign: action.payload
            }
        case 'CREATE_NEW_CAMPAIGN_START':
            return {
                ...state,
                fetching: true,
                campaign: undefined
            }
        case 'GET_CAMPAIGN_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'GET_CAMPAIGN_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                isAuthenticated: true,
                campaign: action.payload
            }
        case 'GET_CAMPAIGN_START':
            return {
                ...state,
                fetching: true,
                campaign: undefined
            }
        case 'DELETE_CAMPAIGN_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'DELETE_CAMPAIGN_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                isAuthenticated: true,
                campaign: action.payload
            }
        case 'DELETE_CAMPAIGN_START':
            return {
                ...state,
                fetching: true,
                campaign: undefined
            }
        case 'FETCH_TARGET_VALUE_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                isAuthenticated: false,
                error: action.payload
            }
        case 'FETCH_TARGET_VALUE_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                isAuthenticated: true,
                targetValue: action.payload
            }
        case 'FETCH_TARGET_VALUE_START':
            return {
                ...state,
                fetching: true,
                targetValue: undefined
            }
        case 'GET_USER_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                error: action.payload
            }
        case 'GET_USER_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                user: action.payload
            }
        case 'GET_USER_START':
            return {
                ...state,
                fetching: true,
                user: undefined
            }

        case 'UPDATE_USER_REJECTED':
            return {
                ...state,
                fetching: false,
                fetched: false,
                error: action.payload
            }
        case 'UPDATE_USER_FULFILLED':
            return {
                ...state,
                fetching: false,
                fetched: true,
                user: action.payload
            }
        case 'UPDATE_USER_START':
            return {
                ...state,
                fetching: true,
                user: undefined
            }
        default:
            return state;
    }

}