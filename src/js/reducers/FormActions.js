import axios from "axios";

const API_URL = 'https://hcjc5541l9.execute-api.eu-west-1.amazonaws.com/prod';

export function addParticipant() {
  return (dispatch, getState) => {

    const form = getState().form;
    const trip = getState().trip.trip;

    var dayString = form.participant.values.dob.slice(0, 2)
    var monthString = form.participant.values.dob.slice(2, 4)
    var yearString = form.participant.values.dob.slice(4, form.participant.values.dob.length)

    var checkedMonthString = Number(monthString)

    var dobMonth

    if (checkedMonthString < 13) {
      dobMonth = checkedMonthString - 1
    } else {
      dobMonth = 0
    }

    var dateDOB = new Date(yearString, dobMonth, dayString, "12", "00", "00").toISOString()

    const participant = {
      trip_id: trip.trip_id,
      event_id: trip.event_id,
      price: trip.price,
      firstName: form.participant.values.firstName,
      lastName: form.participant.values.lastName,
      email: form.participant.values.email,
      mobile: form.participant.values.mobile,
      address: form.participant.values.address,
      postcode: form.participant.values.postcode,
      city: form.participant.values.city,
      dob: dateDOB,
      referral: form.participant.values.referral,
    };
    dispatch({
      type: "ADD_PARTICIPANT",
      payload: participant,
    });
  }
}

export function addPayment() {
  return (dispatch, getState) => {

    const booking = getState().booking.booking;
    const form = getState().form;

    const payment = {
      name: form.payment.values.name,
      cardNumber: form.payment.values.cardNumber,
      expiryMonth: form.payment.values.expiryMonth,
      expiryYear: form.payment.values.expiryYear,
      cvc: form.payment.values.cvc,
    };

    const header = {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': getState().cognito.apiToken,
      }
    }

    const body = {
      "TripId": booking.trip_id,
      "EventId": booking.event_id,
      "Participant": {
        "FirstName": booking.firstName,
        "LastName": booking.lastName,
        "Email": booking.email,
        "Mobilenumber": booking.mobile,
        "InvoiceAddress": {
          "Address": booking.address,
          "Zipcode": booking.postcode,
          "PostPlace": booking.city
        },
        "DateOfBirth": booking.dob,
        "Referral": booking.referral
      },
      "Amount": 6000,
      "Currency": "nok",
      "CardInfo": {
        "CardHolder": payment.name,
        "ExpMonth": payment.expiryMonth,
        "ExpYear": payment.expiryYear,
        "CardNumber": payment.cardNumber,
        "CVC": payment.cvc
      }
    }


    axios.post(API_URL + '/booking', body, header)
        .then(function (response) {
          dispatch({ type: "ADD_PAYMENT" });
        })
        .catch(function (error) {
          dispatch({ type: "ADD_PAYMENT_REJECTED", payload: error })
        });
  }
}

export function addInvoicePayment() {
  return (dispatch, getState) => {
    const booking = getState().booking.booking;

    const header = {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': getState().cognito.apiToken,
      }
    }

    const body = {
      "TripId": booking.trip_id,
      "EventId": booking.event_id,
      "Participant": {
        "FirstName": booking.firstName,
        "LastName": booking.lastName,
        "Email": booking.email,
        "Mobilenumber": booking.mobile,
        "InvoiceAddress": {
          "Address": booking.address,
          "Zipcode": booking.postcode,
          "PostPlace": booking.city
        },
        "DateOfBirth": booking.dob,
        "Referral": booking.referral
      },
      "Amount": 6000,
      "Currency": "nok",
      "CardInfo": {}
    }

    axios.post(API_URL + '/booking', body, header)
        .then(function (response) {
          dispatch({ type: "ADD_PAYMENT" });
        })
        .catch(function (error) {
          dispatch({ type: "ADD_PAYMENT_REJECTED", payload: error })
        });
  }
}

export function toggleChevron() {
  return (dispatch, getState) => {

    const chevronState = { "chevronState": getState().booking.chevronState ? false : true }

    dispatch({ type: "TOGGLE_CHEVRON", payload: chevronState })
  }
}

export function toggleButtonFromCheckbox() {
  return (dispatch, getState) => {

    const termsState = { "termsState": getState().booking.termsState ? false : true }

    dispatch({ type: "TOGGLE_FORM_BUTTON", payload: termsState })
  }
}

export function addSubscriber() {
  return (dispatch, getState) => {

    const form = getState().form;

    const header = {
      headers: {
        'Content-Type': 'application/json',
        'x-api-key': getState().cognito.apiToken,
      }
    }

    const body = {
      email: form.newsletter.values.email,
    };

    axios.post(API_URL + '/newsletter', body, header)
        .then(function (response) {
          dispatch({ type: "ADD_SUBSCRIBER", payload: response });
        })
        .catch(function (error) {
          dispatch({ type: "ADD_SUBSCRIBER_REJECTED", payload: error })
        });
  }
}
