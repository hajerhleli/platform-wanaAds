import axios, { post , get } from 'axios';

const config = {
    headers: {
        'content-type': 'multipart/form-data'
    }
}
const API_URL = 'https://api.platform.wanagames.com';
export function  loginUser(data) {
    return function (dispatch) {
        console.log('************* login user *****************')
        console.log(data)
        post(`${API_URL}/api/auth`, data, config)
            .then(function (response) {
                console.log(response)
                dispatch({type: "FETCH_USER_START", payload: true})
                dispatch({type: "FETCH_USER_FULFILLED", payload: response.data})
                localStorage.setItem('token', response.data.token)
                localStorage.setItem('username', response.data.user.informations.firstName)
                window.location.replace('/campaigns')
            }).catch( function (error) {
                console.log(error.response)
                dispatch({type: "FETCH_USER_REJECTED", payload: error.response.data.message})
            })

    }
}
export function  testLogin() {
    return function (dispatch) {
        console.log('************* user campaigns *****************')
        let token = localStorage.getItem('token')
        if(token !== null) {
            get(`${API_URL}/api/campaigns/my_campaigns?token=${token}`)
                .then(function (response) {
                    dispatch({type: "FETCH_USER_CAMPAIGNS_START", payload: true})
                    dispatch({type: "FETCH_USER_CAMPAIGNS_FULFILLED", payload: response})
                    window.location.replace('/campaigns')
                }).catch(function (error) {
                console.log(error.response)
                if (error.response.status=== 401) {
                    window.location.replace('/login')
                }
                dispatch({type: "FETCH_USER_CAMPAIGNS_REJECTED", payload: error.response.data.message})

            });
        }else{
            window.location.replace('/login')
        }
    }
}
export function  addUser(data) {
    return function (dispatch) {
        console.log('************* add user *****************')
        console.log(data)
        post(`${API_URL}/api/register`, data, config)
            .then(function (response) {
                    dispatch({type: "FETCH_USER_REGISTRATION_START", payload: true})
                    window.location.replace('/login?status=success')
                    dispatch({type: "FETCH_USER_REGISTRATION_FULFILLED", payload: response})

            }).catch( function (error) {
                console.log(error.response)
                dispatch({type: "FETCH_USER_REGISTRATION_REJECTED", payload: error.response.data})

            });
    }
}
export function  getUser() {
    return function (dispatch) {
        console.log('************* get user *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/get_user?token=${token}`)
            .then(function (response) {
                dispatch({type: "GET_USER_START", payload: true})
                dispatch({type: "GET_USER_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status === 401) {
                window.location.replace('/login')
            }
            dispatch({type: "GET_USER_REJECTED", payload: error.response.data.message})
        });
    }
}
export function  updateUser(user) {
    return function (dispatch) {
        console.log('************* update user *****************')
        let token = localStorage.getItem('token')
        post(`${API_URL}/api/user/update?token=${token}`,user)
            .then(function (response) {
                dispatch({type: "UPDATE_USER_START", payload: true})
                dispatch({type: "UPDATE_USER_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status === 401) {
                window.location.replace('/login')
            }
            dispatch({type: "UPDATE_USER_REJECTED", payload: error.response.data.message})
        });
    }
}

export function  getAllCampaigns() {
    return function (dispatch) {
        console.log('************* user campaigns *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/campaigns/my_campaigns?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_USER_CAMPAIGNS_START", payload: true})
                dispatch({type: "FETCH_USER_CAMPAIGNS_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_USER_CAMPAIGNS_REJECTED", payload: error.response.data.message})

        });
    }
}

export function  getAllProducts() {
    return function (dispatch) {
        console.log('************* user product *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/products?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_PRODUCTS_START", payload: true})
                dispatch({type: "FETCH_PRODUCTS_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_PRODUCTS_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  getAllGenders() {
    return function (dispatch) {
        console.log('************* user gender *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/targets/genders?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_GENDERS_START", payload: true})
                dispatch({type: "FETCH_GENDERS_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_GENDERS_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  getAllCategories() {
    return function (dispatch) {
        console.log('************* user categories *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/targets/categories?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_CATEGORIES_START", payload: true})
                dispatch({type: "FETCH_CATEGORIES_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_CATEGORIES_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  getAllInterests() {
    return function (dispatch) {
        console.log('************* user intrests *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/targets/interests?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_INTERESTS_START", payload: true})
                dispatch({type: "FETCH_INTERESTS_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_INTERESTS_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  getAllLanguages() {
    return function (dispatch) {
        console.log('************* user languages *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/targets/languages?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_LANGUAGES_START", payload: true})
                dispatch({type: "FETCH_LANGUAGES_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_LANGUAGES_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  getAllLocations() {
    return function (dispatch) {
        console.log('************* user locations *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/targets/locations?token=${token}`)
            .then(function (response) {
                dispatch({type: "FETCH_LOCATIONS_START", payload: true})
                dispatch({type: "FETCH_LOCATIONS_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "FETCH_LOCATIONS_REJECTED", payload: error.response.data.message})

        });
    }
}
export function  createCampaign(data) {
    return function (dispatch) {
        console.log('************* user create campaign *****************')
        let token = localStorage.getItem('token')
        post(`${API_URL}/api/campaigns/create?token=${token}`, data, config)
            .then(function (response) {
                dispatch({type: "CREATE_NEW_CAMPAIGN_START", payload: true})
                dispatch({type: "CREATE_NEW_CAMPAIGN_FULFILLED", payload: response})
                // window.location.replace('/campaigns')
            }).catch( function (error) {

            console.log(error.response)
            if (error.response.status === 401) {
                window.location.replace('/login')
            }
            dispatch({type: "CREATE_NEW_CAMPAIGN_REJECTED", payload: error.response.data})

        });
    }
}



export function  updateCampaign(data,id) {
    return function (dispatch) {
        console.log('************* user update campaign *****************')
        let token = localStorage.getItem('token')
        post(`${API_URL}/api/campaigns/${id}/update?token=${token}`, data, config)
            .then(function (response) {
                dispatch({type: "UPDATE_CAMPAIGN_START", payload: true})
                dispatch({type: "UPDATE_CAMPAIGN_FULFILLED", payload: response})
                // window.location.replace('/campaigns')
            }).catch( function (error) {

            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "UPDATE_CAMPAIGN_REJECTED", payload: error.response.data.message})

        });
    }
}

export function  getCampaign(id) {
    return function (dispatch) {
        console.log('************* user get campaign *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/campaigns/${id}/show?token=${token}`)
            .then(function (response) {
                dispatch({type: "GET_CAMPAIGN_START", payload: true})
                dispatch({type: "GET_CAMPAIGN_FULFILLED", payload: response})
            }).catch( function (error) {

            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "GET_CAMPAIGN_REJECTED", payload: error.response.data.message})

        });
    }
}


export function  deleteCampaign(id) {
    return function (dispatch) {
        console.log('************* user get campaign *****************')
        let token = localStorage.getItem('token')
        get(`${API_URL}/api/campaigns/${id}/delete?token=${token}`)
            .then(function (response) {
                dispatch({type: "DELETE_CAMPAIGN_START", payload: true})
                dispatch({type: "DELETE_CAMPAIGN_FULFILLED", payload: response})
            }).catch( function (error) {

            console.log(error.response)
            if (error.response.status=== 401) {
                window.location.replace('/login')
            }
            dispatch({type: "DELETE_CAMPAIGN_REJECTED", payload: error.response.data.message})

        });
    }
}


export function  getTargetsValue(data) {
    return function (dispatch) {
        console.log('************* user target value *****************')
        let token = localStorage.getItem('token')
        post(`${API_URL}/api/targets/count?token=${token}`, data, config)
            .then(function (response) {
                dispatch({type: "FETCH_TARGET_VALUE_START", payload: true})
                dispatch({type: "FETCH_TARGET_VALUE_FULFILLED", payload: response})
            }).catch( function (error) {
            console.log(error.response)
            // if (error.response.status=== 401) {
            //     window.location.replace('/login')
            // }
            dispatch({type: "FETCH_TARGET_VALUE_REJECTED", payload: error.response.data.message})

        });
    }
}