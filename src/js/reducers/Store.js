import {applyMiddleware, combineReducers, createStore} from 'redux';
import {reducer as reduxFormReducer} from 'redux-form';
import logger from "redux-logger";
import thunk from "redux-thunk";

import contentReducer from './ContentReducer';

const seilNorgeReducers = combineReducers({
  user: contentReducer,
})

const middleware = applyMiddleware(thunk, logger())

export default createStore(seilNorgeReducers, middleware);
