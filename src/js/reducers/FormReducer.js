export default function reducer(state = {
  fetching: false,
  fetched: false,
  error: null,
  booking: [],
  participantCompleted: false,
  bookingComplete: false,
  bookingFailed: false,
  chevronState: false,
  termsState: true,
  subscribeComplete: false,
}, action) {

  switch (action.type) {
    case 'ADD_PARTICIPANT':
      return {
        ...state,
        participant: action.participant,
        booking: action.payload,
        participantCompleted: true,
        termsState: true,
      }

    case 'ADD_PAYMENT':
      return {
        ...state,
        participantCompleted: false,
        bookingComplete: true,
        termsState: true,
      }
    case 'ADD_PAYMENT_REJECTED':
      return {
        ...state,
        participantCompleted: false,
        bookingComplete: false,
        bookingFailed: true,
        termsState: true,
        error: action.payload
      }
    case 'TOGGLE_CHEVRON':
      return {
        ...state,
        chevronState: action.payload.chevronState,
      }
    case 'TOGGLE_FORM_BUTTON':
      return {
        ...state,
        termsState: action.payload.termsState,
      }
    case 'ADD_SUBSCRIBER':
      return {
        ...state,
        subscribeComplete: true,
      }
    case 'ADD_SUBSCRIBER_REJECTED':
      return {
        ...state,
        subscribeComplete: false,
        error: action.payload,
      }
    default:
      return state;
  }
}
